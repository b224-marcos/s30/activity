// MongoDB Aggregation method
// used to generate manipulated data and perform operations to create filtered results that helps in analyzing data. 

db.fruits.insertMany([
 {
 	"name": "Apple",
 	"color": "Red",
 	"stock": 20,
 	"price": 40,
 	"supplierID": 1,
 	"onSale": true,
 	"origin": ["Philippines", "US"]
 },

 {
 	"name": "Banana",
 	"color": "Red",
 	"stock": 15,
 	"price": 20,
 	"supplierID": 2,
 	"onSale": true,
 	"origin": ["Philippines", "Ecuador"]
 },

 {
 	"name": "Kiwi",
 	"color": "Green",
 	"stock": 25,
 	"price": 50,
 	"supplierID": 1,
 	"onSale": true,
 	"origin": ["US", "China"]
 },

 {
 	"name": "Mango",
 	"color": "Yellow",
 	"stock": 10,
 	"price": 120,
 	"supplierID": 2,
 	"onSale": false,
 	"origin": ["Philippines", "India"]
 }

]);




// ACTIVITY

db.fruits.aggregate ([
	{$match: {"onSale" :true}},
	{$count:"onSale"}
]);


db.fruits.aggregate([
	{$match: {$and: [{onSale: true}, {stock: {$gte: 20}}]} },
	{ $count: "stock" }

]);

db.fruits.aggregate([
	{$match: {"onSale": true}},
	{$group: {_id: "$supplierID", average_price: {$avg: "$price"}}}
]);

db.fruits.aggregate([
	{$match: {"onSale": true}},
	{$group: {_id: "$supplierID", max_price: {$max: "$price"}}}
]);

db.fruits.aggregate([
	{$match: {"onSale": true}},
	{$group: {_id: "$supplierID", min_price: {$min: "$price"}}}
]);